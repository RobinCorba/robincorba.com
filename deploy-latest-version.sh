#!/bin/bash

RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

usage()
{
  echo -e "${RED}Missing mandatory argument${NC}"
  echo "Usage: $0 [-e <test|production>] [-h <sftp host>] [-d <path/in/host>] [-u <ftp user>] [-p ftp password>]" 1>&2;
  exit 1;
}

while getopts ":e:h:d:u:p:" o; do
  case "${o}" in
    'e')
      export ENV=${OPTARG}
      ;;
    'h')
      export SFTP_HOST=${OPTARG}
      ;;
    'd')
      export SFTP_HOST_DIR=${OPTARG}
      ;;
    'u')
      export SFTP_USER=${OPTARG}
      ;;
    'p')
      export SFTP_PASS=${OPTARG}
      ;;
  esac
done
shift $((OPTIND-1))

if [ -z "${ENV}" ] || [ -z "${SFTP_HOST}" ] || [ -z "${SFTP_HOST_DIR}" ] || [ -z "${SFTP_USER}" ] || [ -z "${SFTP_PASS}" ]; then
  usage
fi

echo -e "Starting deployment to ${ENV} environment.."
sleep 2

function uploadfile
{
  echo "      Uploading $1.."
  curl -sS -u ${SFTP_USER}:${SFTP_PASS} --insecure --ftp-create-dirs sftp://${SFTP_HOST}/${SFTP_HOST_DIR}/$1 -T $1
}
export -f uploadfile

cd $(pwd)/src;

echo -e "\n${BLUE}(1/4) ${NC}Transferring library files to ${ENV} server.."
find ./library -type f -exec bash -c 'uploadfile "$0"' {} \;
sleep 1

echo -e "\n${BLUE}(2/4) ${NC}Transferring scripts files to ${ENV} server.."
find ./scripts -type f -exec bash -c 'uploadfile "$0"' {} \;
sleep 1

echo -e "\n${BLUE}(3/4) ${NC}Transferring templates files to ${ENV} server.."
find ./templates -type f -exec bash -c 'uploadfile "$0"' {} \;
sleep 1

echo -e "\n${BLUE}(4/4) ${NC}Transferring root files to ${ENV} server.."
uploadfile ".htaccess"
uploadfile "config.php"
uploadfile "favicon.ico"
uploadfile "index.php"
uploadfile "shootdemdux_highscores.php"
uploadfile "shootdemdux.php"
sleep 1

echo -e "      All files uploaded\n"
sleep 2

echo -e "${GREEN}      Finished deployment!${NC}"
