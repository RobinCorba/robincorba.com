<?php

	session_start();
	require_once("config.php");
	
	// Loading a page
	if(isset($_REQUEST["p"]))
	{
		$p = $class_security->makeSafeString($_GET["p"]);
		$smarty->assign("p", $p);
	}
	else
	{
		$p = "home";
	}
	
	if(file_exists("scripts/".$p.".php"))
	{
		require_once("scripts/".$p.".php");
	}
	elseif(file_exists("templates/".$p.".tpl"))
	{
		$smarty->display($p.".tpl");
	}
	else
	{
		$smarty->display("404.tpl");
	}
	
?>