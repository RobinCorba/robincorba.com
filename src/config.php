<?php

// Read the config.ini file
$loaded_config = parse_ini_file("config.ini", true);

// PHP error handling
ini_set('display_errors', $loaded_config["general"]["debug_mode"]);
ERROR_REPORTING(E_ALL);

// Basic settings
$url            = $loaded_config["site"]["url"];
$root           = __DIR__ . "/";
define("DB_HOST", $loaded_config["database"]["host"]);
define("DB_USER", $loaded_config["database"]["user"]);
define("DB_PASS", $loaded_config["database"]["password"]);
define("DB_NAME", $loaded_config["database"]["name"]);
define("ENV", $loaded_config["general"]["environment"]);

// PHP libraries
require_once('vendor/autoload.php');

require_once("library/class.security.php");
$class_security = new Security();
require_once("library/class.blog.php");
$class_blog     = new Blog();
require_once("library/ayah/ayah.php");
$ayah           = new AYAH();
require_once("library/class.database.php");

// Smarty setup
$smarty = new Smarty();
$smarty->template_dir = $root . "templates/";
$smarty->compile_dir  = $root . "compile/";
if (ENV === "Production") {
    $smarty->caching = 1;
    $smarty->debugging = false;
} else {
    $smarty->caching = 0;
    $smarty->debugging = false;

    if (ENV !== "Development") {
        // Block access if this is the test environment
        if (
            !isset($_SERVER['PHP_AUTH_USER']) ||
            $_SERVER['PHP_AUTH_USER'] !== $loaded_config["auth"]["user"] ||
            $_SERVER['PHP_AUTH_PW'] !== $loaded_config["auth"]["pass"]
        ) {
            header('WWW-Authenticate: Basic realm="Robin Corba Test Environment"');
            header('HTTP/1.0 401 Unauthorized');
            exit('You are not authorized to access this page.');
        }
    }
}

// Miscellaneous setup
$smarty->assign("temp_dir",       $smarty->template_dir);
$smarty->assign("base_dir",       $url);
$smarty->assign("site_title",     $loaded_config["site"]["title"]);
$smarty->assign("site_desc",      $loaded_config["site"]["description"]);
$smarty->assign("site_tags",      $loaded_config["site"]["tags"]);
$smarty->assign("release_year",   $loaded_config["release"]["year"]);
$smarty->assign("tinymce_apikey", $loaded_config["3rdparties"]["tinymce-apikey"]);
$smarty->assign("ENV",            ENV);
