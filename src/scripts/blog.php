<?php

    if(isset($_POST["comment"]))
    {
        if(array_key_exists('blog_post_button', $_POST))
        {
            // Validate the ReCAPTCHA response
            $grecaptcha_secret  = "6LdJiBwTAAAAAFiTD9avxcJD7xLMVCXt7NbDPEOr";
            $response           = $_POST["g-recaptcha-response"];
            $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$grecaptcha_secret."&response=".$response."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
            if($response['success'] == false)
            {
                echo "Sorry, but we were not able to verify you as human. Please try again";
            }
            else
            {
                if($_POST["author_name"] != "" && $_POST["comment"] != "")
                {
                    $data["post_id"]    = $class_security->makeSafeNumber($_POST["post_id"]);
                    $data["name"]       = $class_security->makeSafeString($_POST["author_name"]);
                    $data["comment"]    = $class_security->makeSafeString($_POST["comment"]);
                    $class_blog->postComment($data);
                }
                else
                {
                    echo "Sorry, but we were not able to verify you as human. Please try again";
                }
            }
        }
    }

    // Browse archive
    for($y = date("Y"); $y > 2006; $y--)
    {
        $archive_years[] = $y;
    }
    $smarty->assign("archive_years", $archive_years);

    if(isset($_REQUEST["archive"]))
    {
        $year = $class_security->makeSafeNumber($_REQUEST["archive"]);
        $smarty->assign("articles",         $class_blog->retrieveArchive($year));
        $smarty->assign("searched_year",    $year);
        $smarty->assign("seo_title",        "Archive of $year");
        $smarty->assign("seo_desc",         "An archive of stories written by Robin Corba in $year.");
        $smarty->display("blog_archive.tpl", $year);
    }
    else
    {
        if(isset($_REQUEST["id"]))
        {
            $post_id	= $class_security->makeSafeNumber($_REQUEST["id"]);
            $post		= $class_blog->retrieveArticle($post_id, false);
        }
        elseif(isset($_REQUEST["title"]))
        {
            if($_REQUEST["title"] == "")
            {
                // robincorba.com/blog/ was called, retrieve the most recent article
                $post = $class_blog->retrieveLastArticle();
            }
            else
            {
                $post = $class_blog->retrieveArticle($class_security->makeSafeString($_REQUEST["title"]), true);
            }
            $post_id = $post["post_id"];
        }
        else
        {
            $post = $class_blog->retrieveLastArticle();
            if($post === false)
            {
                // If no article exists, just show the template
                $smarty->display("blog_post.tpl");
                exit();
            }
            $post_id = $post['post_id'];
            header("Location: ".$url."blog/".$post["post_title_slug"]);
        }

        // Retrieve comments
        $smarty->assign("blog_comments", $class_blog->retrieveComments($post_id));

        // Recent posts
        $smarty->assign("recent_posts", $class_blog->retrieveRecentArticles(3, $post_id));

        // Prepare a Facebook thumbail
        $fb_thumb	= substr_count($post["post_content"], 'id="fb_thumb"');
        if($fb_thumb > 0)
        {
            $thumb_start	= strpos($post["post_content"],'id="fb_thumb"');
            $src_start		= strpos($post["post_content"],'src="',$thumb_start) + 5;
            $src_end		= strpos($post["post_content"],'"',$src_start);
            $img_src		= substr($post["post_content"], $src_start, $src_end - $src_start);
            $smarty->assign("fb_thumb",$img_src);
        }
        $smarty->assign("seo_title",    $loaded_config["site"]["title"]." - ".$post["post_title"]);
        $smarty->assign("seo_desc",     $post["post_desc"]);
        $smarty->assign("seo_tags",     $post["post_tags"]);
        $smarty->assign("post",         $post);
        $smarty->display("blog_post.tpl", $post_id);
    }

?>
