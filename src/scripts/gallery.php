<?php
	// SEO
	$seo["title"]	= "Photo Gallery";
	$seo["desc"]	= "A photo gallery of Robin Corba's best pictures.";
	
	$smarty->assign("seo",$seo);
	$smarty->display("header.tpl");
	$smarty->display("gallery.tpl");
?>