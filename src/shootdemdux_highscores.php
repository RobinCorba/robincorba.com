<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<link rel="icon" href="/favicon.ico" />
		<title>Shoot dem Dux - Leaderboards</title>
		<meta name="description" content="{$seo_desc}" />
		<link rel="shortcut icon" href="favicon.ico" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no", target-densitydpi=device-dpi />
		<style>
			body
			{
				background:		#E2F4FE;
				color:				#444;
				font-family:		Verdana, Sans-serif;
				font-size:			16px;
			}
			
			table thead tr td
			{
				background:		#B00;
				color:				#FFF;
				padding:			15px;
			}
			
			table tbody tr td
			{
				padding:			15px;
			}
			
			table tbody tr:nth-child(even) td
			{
				background:	#B1E2FC;
			}
		</style>
    </head>
	<body>
		<h1>Shoot dem Dux - Leaderboards</h1>
<?php
	
	// Shoot Dem Dux Highscores
	require_once("library/class.database.php");
	//require_once("library/class.security.php");
	//$class_security = new Security();
	
	Database::connect();
	$r = mysql_query("SELECT * FROM android_shootdemdux_highscores ORDER BY score_ducks DESC;");
	Database::disconnect();
	
	echo"<table cellpadding='0' cellspacing='0' style='width:100%;'><thead><tr><td>Rank</td><td>Name</td><td>Kills</td></tr><tbody>";
	$rank = 0;
	while($s = mysql_fetch_assoc($r))
	{
		$rank++;
		echo"<tr><td style='width:20%;'>$rank</td><td style='width:60%;'>$s[score_player_name]</td><td style='width:20%;'>$s[score_ducks]</td></tr>";
	}
	echo "</tbody></table>";

?>
	</body>
</html>