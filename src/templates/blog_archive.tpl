{include file="header.tpl"}
<div id="blog_archive">
	<h1>Archive of {$searched_year}</h1>
	{if $articles}
		{foreach from=$articles item="a"}
		<a href="{$base_dir}blog/{$a.post_title_slug}">
			<h1>{$a.post_title}</h1>
			<p class="date">{$a.post_date_published_real}</p>
			<p class="desc">{$a.post_desc}</p>
		</a>
		{/foreach}
	{else}
		<p>Sorry, no articles were written in this year.</p>
	{/if}
</div>
{include file="blog_sidepanel.tpl"}