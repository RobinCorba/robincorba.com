{if isset($post.post_id)}
<div id="blog_comments">
	<h1>Comments</h1>
	{if $blog_comments}
		{foreach from=$blog_comments item="c"}
			<h2>{$c.comment_author}</h2>
			<p>{$c.comment_date}</p>
			<span>{$c.comment_content}</span>
		{/foreach}
	{else}
		<em>No comments posted yet.</em>
	{/if}
	<script src="https://cdn.tiny.cloud/1/{$tinymce_apikey}/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	{literal}
	<script type="text/javascript">
    tinymce.init({
        selector: '#comment',
        menubar: "",
        toolbar: "bold italic underline",
    });
		
		/*window.onload = function()
		{
			document.getElementById("loading_comments").setAttribute("style", "display:none;");
		};*/
		
		function getCommentContent()
		{
			$('textarea#comment').html(tinymce.get('comment').getContent());
		}
		
	</script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
	{/literal}
	<b>Post a comment</b>
	<form method="post" action="">
		<!--div id="loading_comments">Loading comments..</div-->
		<input type="hidden" name="post_id" maxlength="6" value="{$post.post_id}" />
		<input type="text" name="author_name" maxlength="32" placeholder="Your name.." />
		<textarea id="comment" name="comment" style="width:100%; height:60px;"></textarea>
		<div class="g-recaptcha" data-sitekey="6LdJiBwTAAAAABhsZxxVl7W5OcBWNhk_IlwDrTOg"></div>
		<input type="submit" name="blog_post_button" onclick="javascript:getCommentContent();" value="Post comment" />
	</form>
    
</div>
{/if}

{if !empty($recent_posts)}
<div id="recent_posts">
	<h1>Other recent articles</h1>
	{foreach from=$recent_posts item="p"}
		<a href="{$base_dir}blog/{$p.post_title_slug}" class="summary">
			<h2>{$p.post_title}</h2>
			<span>{$p.post_desc}</span>
		</a>
	{/foreach}
</div>
{/if}

<div id="browse_archive">
	<h1>Archive</h1>
	<ul>
		{foreach from=$archive_years item="y"}
		<li><a href="{$base_dir}blog/archive/{$y}">Stories from {$y}</a></li>
		{/foreach}
	</ul>
</div>

{if !empty($related_posts)}
<div id="related_posts">
	<h1>Related</h1>
	{foreach from=$related_posts item="p"}
		<a href="{$base_dir}blog/{$p.post_title_slug}" class="summary">
			<h2>{$p.post_title}</h2>
			<span>{$p.post_desc}</span>
		</a>
	{/foreach}
</div>
{/if}