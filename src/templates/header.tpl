<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>{if !empty($seo_title)}{$seo_title}{else}{$site_title}{/if}</title>
    <!-- Meta tags -->
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="{if !empty($seo_desc)}{$seo_desc}{else}{$site_desc}{/if}" />
		<meta name="keywords" content="{if !empty($seo_tags)}{$seo_tags}{else}{$site_tags}{/if}" />
		{if $ENV == "Production"}
		<meta name="robots" content="follow, index" />
		{else}
		<meta name="robots" content="noindex" />
		{/if}
		<meta name="copyright" content="Robin Corba 2007-{$release_year}" />
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
    <!-- Styling -->
		<link rel="shortcut icon" type="" href="{$base_dir}favicon.ico" />
		<link href="{$base_dir}templates/style_basics.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="{$base_dir}templates/style_320.css" rel="stylesheet" type="text/css" media="screen and (max-width:639px)" />
		<link href="{$base_dir}templates/style_640.css" rel="stylesheet" type="text/css" media="screen and (min-width:640px) and (max-width:799px)" />
		<link href="{$base_dir}templates/style_800.css" rel="stylesheet" type="text/css" media="screen and (min-width:800px) and (max-width:1023px)" />
		<link href="{$base_dir}templates/style_1024.css" rel="stylesheet" type="text/css" media="screen and (min-width:1024px) and (max-width:1279px)" />
		<link href="{$base_dir}templates/style_1280.css" rel="stylesheet" type="text/css" media="screen and (min-width:1280px)" />
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		{include file="google_analytics.tpl"}
	</head>
	<body>
		<ul id="nav">
			<li><a href="{$base_dir}">Home</a></li>
			<li><a href="{$base_dir}blog">Blog</a></li>
		</ul>