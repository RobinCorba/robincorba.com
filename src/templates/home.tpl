{include file="header.tpl"}
<div id="home_banner">
	<h1 id="whoami">Hello, my name is Robin Corba</h1>
	<p id="whatami">I am a multimedia web developer and project manager</p>
	<p id="myquote">"I just like to keep things simple"</p>
</div>
<div id="whatcanido">
	<h1>What can I do for you?</h1>
	<div class="list">
		<h2>Create..</h2>
		<ul>
			<li>Responsive websites</li>
			<li>Mobile applications</li>
			<li>Facebook applications</li>
			<li>Content Management System (CMS)</li>
			<li>Flash products</li>
			<li>Desktop &amp; Web games</li>
		</ul>
	</div>
	<div class="list">
		<h2>Do..</h2>
		<ul>
			<li>Search Engine Optimization (SEO)</li>
			<li>Website traffic analysis</li>
			<li>Web performance analysis</li>
			<li>Usability testing</li>
		</ul>
	</div>
</div>
<div id="socmed">
	<h1>Contact me</h1>
	<a href="mailto:info@robincorba.com" id="email">Send me an e-mail<br /><span>info@robincorba.com</span></a>
	<a href="http://www.facebook.com/R.Corba" id="fb" target="_new">Find me on Facebook<br /><span>facebook.com/R.Corba</span></a>
	<a href="http://www.linkedin.com/pub/robin-corba/30/270/14" id="linkedin" target="_new">View my LinkedIn profile</a>
</div>
<!--div id="twitter_feed">
	<h1>Recent tweets</h1>
	<a class="twitter-timeline" href="https://twitter.com/RobinCorba" data-widget-id="303530922819584001">Tweets by @RobinCorba</a>
	{literal}
	<script type="text/javascript">
	!function(d,s,id)
	{
		var js,fjs = d.getElementsByTagName(s)[0];
		if(!d.getElementById(id))
		{
			js = d.createElement(s);
			js.id = id;
			js.src = "//platform.twitter.com/widgets.js";
			fjs.parentNode.insertBefore(js,fjs);
		}
	}
	(
		document,
		"script",
		"twitter-wjs"
	);
	</script>
	{/literal}
</div-->