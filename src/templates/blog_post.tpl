{include file="header.tpl"}

{if !empty($post)}
<script src="{$base_dir}templates/js/modal.js"></script>
<link href="{$base_dir}templates/modal.css" rel="stylesheet" type="text/css" media="screen" />

<div id="blog_post">
	<p id="date">{$post.post_date_published_real}</p>
	<h1>{$post.post_title}</h1>
	<p id="summary">{$post.post_desc}</p>
	<div id="content">
        {if !empty($post.post_picture) && $post.post_picture != "default_picture.jpg"}<img id="post_picture" src="{$base_dir}media/images/news/{$post.post_picture}" alt="{$post.post_title}" />{/if}
        {$post.post_content}
    </div>
</div>

<div id="blog_modal" class="modal">
  <span id="modal_close">&times;</span>
  <img class="modal-content" id="modal_image" src="{$base_dir}media/images/news/default_picture.jpg" >
  <div id="modal_caption"></div>
</div>

{else}
	<div id="blog_post">
		<h1>Not found</h1>
		<p>Sorry, no article was found.</p>
	</div>
{/if}
{include file="blog_sidepanel.tpl"}