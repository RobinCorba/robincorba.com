<?php
	class DB
	{
        private static $db_link;
        private static $db_error;
        private static $db_insert_id;
        
		private static function connect()
		{
            self::$db_link = mysqli_connect(DB_HOST, DB_USER, DB_PASS);
            if (self::$db_link->connect_error) {
                die('Error connecting to '.DB_SERVER_NAME.' server: ' . self::$db_link->connect_error);
            }
			mysqli_select_db(self::$db_link, DB_NAME) or die ("Error selecting the database: " . mysqli_error(self::$db_link));
		}
		
		private static function disconnect()
		{
			mysqli_close(self::$db_link);
		}
        
        public static function query($q_string)
		{
            self::connect();
            $r = mysqli_query(self::$db_link, $q_string);
            self::$db_error     = mysqli_error(self::$db_link);
            self::$db_insert_id = mysqli_insert_id(self::$db_link);
            self::disconnect();
            return $r;
        }
        
        public static function getError()
		{
            return self::$db_error;
        }
        
        public static function getInsertID()
		{
            return self::$db_insert_id;
        }
	}
?>