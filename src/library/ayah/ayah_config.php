<?php
// Edit the two lines below to use the keys for your site.
// (Note: you can find your keys at http://portal.areyouahuman.com/dashboard)
define( 'AYAH_PUBLISHER_KEY', '414fde7fc310d165d6d6a776c68cdf64eaadc300');
define( 'AYAH_SCORING_KEY', 'bef49460958f6e87c6ed862431a170f156f9832c');


// Set defaults for values needed by the ayah.php file.
// (Note: you do not need to change these.)
define( 'AYAH_WEB_SERVICE_HOST', 'ws.areyouahuman.com');
define( 'AYAH_TIMEOUT', 0);
define( 'AYAH_DEBUG_MODE', FALSE);
define( 'AYAH_USE_CURL', TRUE);
