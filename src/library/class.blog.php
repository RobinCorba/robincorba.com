<?php
	
	class Blog
	{
        /* Function retrieveArticle
        *  Retrieves an article based on the Article ID or the url-friendly name
        *  @param string|int $key the Article ID (integer) or slug (String)
        *  @param boolean $based_on_slug If set to TRUE it means the key is the slug, default is FALSE which means the key is the Article ID
        *  @returns array An array with the article or FALSE if no article is found
        */
		function retrieveArticle($key, $based_on_slug = false)
		{
            if($based_on_slug)
            {
                $where_clause = "WHERE post_title_slug = '$key'";
            }
            else
            {
                $where_clause = "WHERE post_id = '$key'";
            }
            
            $result = DB::query("SELECT
                            post_id,
                            post_title,
                            post_title_slug,
                            post_desc,
                            post_tags,
                            post_picture,
                            post_content,
                            post_date_published
                        FROM news_posts
                        ".$where_clause."
                        LIMIT 0,1;");
            
            // Failed to execute the query
			if($result === false)
			{
				return false;
			}
			else
			{
				$r = mysqli_fetch_assoc($result);
                if($r["post_id"] == "")
				{
                    // No article was found
					return false;
				}
				else
				{
                    // Return the article
                    $r["post_date_published_real"]  = date("j F Y", strtotime($r["post_date_published"]));
                    $r["post_time_published_real"]  = date("H:i", strtotime($r["post_date_published"]));
                    return $r;
                }
			}
		}
		
        /* Function retrieveLastArticle
        *  Retrieves the most recent published article
        *  @returns array An array with the article or FALSE if no article is found
        */
		function retrieveLastArticle()
		{
			// TODO check why news_posts is not created by the CMS
			$result = DB::query("SELECT post_id FROM news_posts
                                 WHERE post_status = 'published'
                                 ORDER BY post_date_published DESC LIMIT 0,1;");
			if($result === false)
			{
				return false;
			}
			$r = mysqli_fetch_assoc($result);
			if(empty($r["post_id"]))
			{
				return false;
			}
			return $this->retrieveArticle($r["post_id"], false);
		}
		
		function retrieveRecentArticles($amount = 3, $current_id)
		{
			$r = DB::query("SELECT
                    post_id,
                    post_title,
                    post_title_slug,
                    post_desc
                FROM news_posts
                WHERE post_id != '$current_id' AND post_status = 'published'
                ORDER BY post_date_published DESC LIMIT 0,$amount;");
			if($r === false)
			{
				return false;
			}
			else
			{
				$all_p = null;
				while($p = mysqli_fetch_assoc($r))
				{
					$all_p[] = $p;
				}
				if($all_p === null)
				{
					return false;
				}
				else
				{
					return $all_p;
				}
			}
		}
		
        /* Function retrieveArchive
        *  Retrieves all articles published in given year
        *  @param int $year The year to search for
        *  @returns array An array with all articles found for the year or FALSE if none are found
        */
		function retrieveArchive($year)
		{
			$first_day = mktime(00,00,00,01,01,$year);
			$last_day = mktime(23,59,59,12,31,$year);
			$result = DB::query("SELECT
                    post_id,
                    post_title,
                    post_title_slug,
                    post_desc,
                    post_tags,
                    post_date_published
                FROM news_posts
                WHERE post_date_published BETWEEN '".$year."-01-01 00:00:00' AND '".$year."-12-31 23:59:59'
                ORDER BY post_date_published ASC;");
			$all_articles = array();

			// In case the table or structure is missing or incorrect
			if($result === false)
			{
				return false;
			}
			while($article = mysqli_fetch_assoc($result))
			{
				$article["post_date_published_real"]    = date("j F Y", strtotime($article["post_date_published"]));
				$article["post_time_published_real"]    = date("H:i", strtotime($article["post_date_published"]));
				$all_articles[] = $article;
			}
            
            // Return all found articles
			if( empty($all_articles) )
			{
				return false;
			}
			else
			{
				return $all_articles;
			}
		}
		
		function retrieveComments($post_id)
		{
			$r = DB::query("SELECT * FROM blog_comments WHERE comment_post_id = '$post_id' ORDER BY comment_date DESC;");
			if($r === false)
			{
				return false;
			}
			else
			{
				$posts = null;
				while($p = mysqli_fetch_assoc($r))
				{
					$p["comment_date"] = date("j F Y", $p["comment_date"]) . " (" . date("H:i", $p["comment_date"]) . " hrs)";
					$posts[] = $p;
				}
				if($posts === null)
				{
					return null;
				}
				else
				{
					return $posts;
				}
			}
		}
		
		function postComment($data)
		{
			DB::query("INSERT INTO blog_comments (comment_post_id, comment_author, comment_content, comment_author_ip, comment_date) VALUES ('$data[post_id]', '$data[name]', '$data[comment]', '".$_SERVER["REMOTE_ADDR"]."', '".time()."');");
		}
	}
	
?>