# robincorba.com

The source code of my personal website

## Installation

1. Have `docker` and `docker-compose` installed on your machine.
1. Make an `.env` file based on `.env.example`
1. Run `docker-compose up`
1. Log into the app container with `docker exec -it robincorba-site /bin/bash`
1. Install PHP dependencies with `composer install`
1. Add `www.robincorba.test` to `/etc/hosts` for 127.0.0.1
1. Access PHPMyAdmin on `http://localhost:8080` and import file `./db/testdata.sql.gz` into `corba_cms` database
1. Access development website on `http://www.robincorba.test/`
1. Happy coding!
