FROM php:7.4-apache
LABEL maintainer="Robin Corba <info@robincorba.com>"

VOLUME /var/www/html

# Install PHP extention for MySQL
RUN docker-php-ext-install mysqli

# Enable the rewrite mod
RUN a2enmod rewrite

# Install Composer
ENV COMPOSER_VERSION 2.0.14
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}

# Install handy bash aliases
COPY /config/.bashrc /root/.bashrc
